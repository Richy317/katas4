const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function addToPage(text) {
    const newDiv = document.createElement('div')
    newDiv.textContent = text
    container.appendChild(newDiv)
}

function one() {
    addToPage(`one: ${JSON.stringify(gotCitiesCSV.split(','))}`)
}
one()

function two() {
    addToPage(`two: ${JSON.stringify(bestThing.split(' '))}`)
}
two()

function three() {
    let re = /,/gi
    addToPage(`three: ${JSON.stringify(gotCitiesCSV.replace(re, '; '))}`)
}
three()

function four() {
    addToPage(`four: ${JSON.stringify(lotrCitiesArray.join(', '))}`)
}
four()

function five() {
    addToPage(`five: ${JSON.stringify(lotrCitiesArray.slice(0,5))}`)
}
five()

function six() {
    addToPage(`six: ${JSON.stringify(lotrCitiesArray.slice(3,8))}`)
}
six()

function seven() {
    addToPage(`seven: ${JSON.stringify(lotrCitiesArray.slice(2,5))}`)
}
seven()

function eight() {
    lotrCitiesArray.splice(2,1)
    addToPage(`eight: ${JSON.stringify(lotrCitiesArray)}`)
}
eight()

function nine() {
    lotrCitiesArray.splice(5,3)
    addToPage(`nine: ${JSON.stringify(lotrCitiesArray)}`)
}
nine()

function ten() {
    lotrCitiesArray.splice(2,0,'Rohan')
    addToPage(`ten: ${JSON.stringify(lotrCitiesArray)}`)
}
ten()

function eleven() {
    lotrCitiesArray.splice(5,1,'Deadest Marshes')
    addToPage(`eleven: ${JSON.stringify(lotrCitiesArray)}`)
}
eleven()

function twelve() {
    addToPage(`twelve: ${JSON.stringify(bestThing.slice(0,14))}`)
}
twelve()

function thirteen() {
    addToPage(`thirteen: ${JSON.stringify(bestThing.slice(-12))}`)
}
thirteen()

function fourteen() {
    addToPage(`fourteen: ${JSON.stringify(bestThing.slice(23,38))}`)
}
fourteen()

function fifthteen() {
    addToPage(`fifthteen: ${JSON.stringify(bestThing.substring(bestThing.length - 12))}`)
}
fifthteen()

function sixteen() {
    addToPage(`sixteen: ${JSON.stringify(bestThing.substring(23,38))}`)
}
sixteen()

function seventeen() {
    addToPage(`seventeen: ${JSON.stringify(bestThing.indexOf('only'))}`)
}
seventeen()

function eighteen() {
    addToPage(`eighteen: ${JSON.stringify(bestThing.indexOf('bit'))}`)
}
eighteen()

function nineteen() {
    let newArr = gotCitiesCSV.split(',')
    const arr = []
    const vowels = ['aa', 'ee', 'ii', 'oo', 'uu']
    for (let city of newArr) {
        for (let vowel of vowels) {
            if (city.includes(vowel)) {
                arr.push(city)
        }
        }
    }
    addToPage(`nineteen: ${JSON.stringify(arr)}`)
}
nineteen()

function twenty() {
    const arr = []
    const contain = ['or']
    for (let city of lotrCitiesArray) {
        if (city.endsWith(contain)) {
            arr.push(city)
        }
    }
    addToPage(`twenty: ${JSON.stringify(arr)}`)
}
twenty()

function twentyOne() {
    const newArr = bestThing.split(' ')
    const arr = []
    for (let word of newArr) {
        if (word.startsWith('b')) {
            arr.push(word)
        }
    }
    addToPage(`twentyOne: ${JSON.stringify(arr)}`)
}
twentyOne()

function twentyTwo() {
    if (lotrCitiesArray.includes('Mirkwood')) {
        response = 'Yes'
    } else {
        response = 'No'
    }
    addToPage(`twentyTwo: ${JSON.stringify(response)}`)
}
twentyTwo()

function twentyThree() {
    if (lotrCitiesArray.includes('Hollywood')) {
        response = 'Yes'
    } else {
        response = 'No'
    }
    addToPage(`twentyThree: ${JSON.stringify(response)}`)
}
twentyThree()

function twentyFour() {
    addToPage(`twentyFour: ${JSON.stringify(lotrCitiesArray.indexOf('Mirkwood'))}`)
}
twentyFour()

function twentyFive() {
    const arr = []
    for (let name of lotrCitiesArray) {
        if (name.includes(' ')) {
            arr.push(name)
        }
    }
    addToPage(`twentyFive: ${JSON.stringify(arr)}`)
}
twentyFive()

function twentySix() {
    addToPage(`twentySix: ${JSON.stringify(lotrCitiesArray.reverse())}`)
}
twentySix()

function twentySeven() {
    addToPage(`twentySeven: ${JSON.stringify(lotrCitiesArray.sort())}`)
}
twentySeven()

function twentyEight() {
    lotrCitiesArray.sort(function(a,b) {
        return a.length - b.length
    })
    addToPage(`twentyEight: ${JSON.stringify(lotrCitiesArray)}`)
}
twentyEight()

const lastCity = lotrCitiesArray.pop()

function twentyNine() {
    addToPage(`twentyNine: ${JSON.stringify(lotrCitiesArray)}`)
}
twentyNine()

function thirty() {
    lotrCitiesArray.push(lastCity)
    addToPage(`thirty: ${JSON.stringify(lotrCitiesArray)}`)
}
thirty()

const firstCity = lotrCitiesArray.shift()

function thirtyOne() {
    addToPage(`thirtyOne: ${JSON.stringify(lotrCitiesArray)}`)
}
thirtyOne()

function thirtyTwo() {
    lotrCitiesArray.unshift(firstCity)
    addToPage(`thirtyTwo: ${JSON.stringify(lotrCitiesArray)}`)
}
thirtyTwo()
